#!/bin/bash
# This bash script will clone a repository with all the branch.
function repoclone {

        echo -n "Enter the Git repository URL : "
        read  url

	if [ "$url" = '' ]; then
	     echo "Please give URL "
	     exit 0
	fi

        reponame=$(echo $url | awk -F/ '{print $NF}' | sed -e 's/.git$//')

        git clone $url

        cd $reponame

        for branch in $(git branch -a | grep '^\s*remotes' | egrep --invert-match '(:?HEAD|master)$'); do
            git branch --track "${branch##*/}" "$branch"
        done

}

repoclone
